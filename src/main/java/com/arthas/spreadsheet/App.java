package com.arthas.spreadsheet;

import com.arthas.spreadsheet.entity.SpreadSheet;

public class App {
	
	public static void main(String[] args) {
		if (args.length == 0 || args[0].isEmpty())
		{
			System.out.println("!!! Enter file name !!!!");
			return;
		}
		
		SpreadSheet sheet = SpreadSheet.getInctance();
		String fileName = args[0];
		
		sheet.defaultInput(fileName);
		sheet.defaultOutput();
	}
}
