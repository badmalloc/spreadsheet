package com.arthas.spreadsheet.entity;


/**
 * Class for cells with test
 * @author Arthas
 *
 */
public class TextCell extends BaseCell <String>{

	public TextCell() {
		super();
		super.setCalculaded(true);
	}
	
	@Override
	public void setCalculaded(boolean calculaded) {	
	}
	
	@Override
	public String getExpression() {
		return String.valueOf(super.getValue());
	}
	
	@Override
	public void setExpression(String expression) {
		super.setExpression(expression);
	}
	
}
