package com.arthas.spreadsheet.entity;

/**
 * Base abstract generic class for all types of cells
 * 
 * @author Arthas
 * 
 */

public abstract class BaseCell <T>{

	public BaseCell() {
	}
	
	public BaseCell(T value) {
		this.value = value;
	}

	private T value;
	private String expression;
	private boolean calculaded;

	/**
	 * Get expression for calculation
	 * 
	 * @return {@link String}
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * Get cell value
	 * 
	 * @return {@link Object}
	 */
	public T getValue() {
		return value;
	}

	/**
	 * Check is cell calculated
	 * 
	 * @return {@link boolean}
	 */
	public boolean isCalculaded() {
		return calculaded;
	}

	public void setCalculaded(boolean calculaded) {
		this.calculaded = calculaded;
	}

	/**
	 * Set expression for calculation
	 * 
	 * @param expression
	 *            {@link String}
	 */
	public void setExpression(String expression) {
		this.expression = expression;
	}

	/**
	 * Set cell value
	 * 
	 * @param value
	 *            {@link Object}
	 */
	public void setValue(T value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value.toString();
	}

}
