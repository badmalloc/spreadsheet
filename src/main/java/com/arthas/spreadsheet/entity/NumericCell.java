package com.arthas.spreadsheet.entity;

/**
 * Class for cells with numbers
 * @author Arthas
 *
 */
public class NumericCell extends BaseCell<Integer> {

	public NumericCell() {
		super();
		super.setCalculaded(true);
		super.setValue(0);
	}
	
	public NumericCell(Integer value) {
		this();
		super.setValue(value);
	}
		
	@Override
	public String getExpression() {
		return String.valueOf(super.getValue());
	}
	
	@Override
	public void setExpression(String expression) {
		// TODO Auto-generated method stub
		super.setValue(Integer.valueOf(expression));
	}
	
	@Override
	public void setCalculaded(boolean calculaded) {
	}
	
	
	

}
