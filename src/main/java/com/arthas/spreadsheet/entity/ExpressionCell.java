package com.arthas.spreadsheet.entity;

import org.apache.commons.lang3.StringUtils;

import com.arthas.spreadsheet.exceptions.ExpressionException;
import com.arthas.spreadsheet.exceptions.ProcessingCalcException;

/**
 * Class for cells with expressions
 * 
 * @author Arthas
 * 
 */
public class ExpressionCell extends BaseCell<String> {

	private boolean busy;
	private String result;

	private String[] operators = { "+", "-", "*", "/" };

	public ExpressionCell() {
		super();
		this.setCalculaded(false);
		this.busy = false;
	}

	@Override
	public String getValue() {
		if (this.isCalculaded()) {
			return result;
		}

		try {
			if (this.busy) {
				throw new ProcessingCalcException();
			}

			this.busy = true;
			String strExpression = this.getExpression();

			int index = StringUtils.indexOfAny(strExpression, operators);
			Integer resultValue = 0;

			if (index == 0) {
				throw new ExpressionException();
			}

			String cellAddr = "";
			if (index < 0)
				cellAddr = strExpression;
			else {
				cellAddr = strExpression.substring(0, index);
				strExpression = strExpression.substring(index);
			}

			resultValue = Integer.valueOf(SpreadSheet
					.getCellValueByAddr(cellAddr));

			while (index > -1) {
				char operator = strExpression.charAt(0);
				strExpression = strExpression.substring(1);
				index = StringUtils.indexOfAny(strExpression, operators);
				if (index < 0)
					cellAddr = strExpression;
				else {
					cellAddr = strExpression.substring(0, index);
					strExpression = strExpression.substring(index);
				}

				Integer cellValue = Integer.valueOf(SpreadSheet
						.getCellValueByAddr(cellAddr));
				switch (operator) {
				case '+':
					resultValue += cellValue;
					break;
				case '-':
					resultValue -= cellValue;
					break;
				case '*':
					resultValue *= cellValue;
					break;
				case '/':
					resultValue /= cellValue;
					break;
				default:
					throw new ExpressionException();
				}
			}

			result = resultValue.toString();
		} catch (ProcessingCalcException e) {
			result = e.getMessage();
		} catch (ExpressionException e) {
			result = e.getMessage();
		} finally {
			this.busy = false;
			this.setCalculaded(true);
		}

		return result;
	}

	@Override
	public void setValue(String value) {
		super.setValue(value);
		this.setCalculaded(true);
	}

	@Override
	public String toString() {
		return this.getValue();
	}
}
