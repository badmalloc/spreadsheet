package com.arthas.spreadsheet.entity;


/**
 * Class for empty cells
 * @author Arthas
 *
 */
public class EmptyCell extends BaseCell <String>{

	public EmptyCell() {
		super();
		super.setCalculaded(true);
	}

	@Override
	public String getExpression() {
		return "";
	}

	@Override
	public String getValue() {
		return ""; // TODO : throw error
	}

	@Override
	public void setExpression(String expression) {

	}

	@Override
	public void setValue(String value) {
	}

	@Override
	public String toString() {
		return "";
	}
	
	@Override
	public void setCalculaded(boolean calculaded) {
	}
}
