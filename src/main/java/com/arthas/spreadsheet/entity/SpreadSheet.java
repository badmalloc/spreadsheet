package com.arthas.spreadsheet.entity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.arthas.spreadsheet.exceptions.NotDefinedCell;
import com.arthas.spreadsheet.exceptions.RangeException;
import com.arthas.spreadsheet.exceptions.UnknownCellException;

/**
 * Class of spreadsheet
 * 
 * @author arthas
 * 
 */
public class SpreadSheet {

	private static SpreadSheet instance;

	/**
	 * Thread safe method for getting instance of spreadsheet
	 * 
	 * @return {@link SpreadSheet}
	 */
	public static SpreadSheet getInctance() {
		if (instance == null) {
			synchronized (SpreadSheet.class) {
				if (instance == null) {
					instance = new SpreadSheet();
				}
			}
		}
		return instance;
	}

	/**
	 * List of cells, witch used for calculation
	 * 
	 * I think, it would be better to use HashMap for holding data, it would
	 * work better on big number of data
	 * 
	 */

	private List<BaseCell> cells;
	private int cols;
	private int rows;

	/**
	 * Return number of columns
	 * 
	 * @return {@link int}
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * Set columns count
	 * 
	 * @param cols
	 */
	public void setCols(int cols) {
		this.cols = cols;
	}

	/**
	 * Return number of rows
	 * 
	 * @return {@link int}
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * Set rows count
	 * 
	 * @param rows
	 */
	public void setRows(int rows) {
		this.rows = rows;
	}

	private SpreadSheet() {
		this.cells = new ArrayList<BaseCell>();
	}

	/**
	 * Collection of spreadsheet elements
	 * 
	 * @return {@link List}<{@link BaseCell}>
	 */
	public List<BaseCell> getCells() {
		return this.cells;
	}

	/**
	 * Getting specific cell by row and col
	 * 
	 * @param row
	 * @param col
	 * @return {@link BaseCell}
	 */
	public BaseCell getCell(int row, int col) {
		try {
			if (row >= getRows() || col >= getCols())
				throw new RangeException();
			
			int index = row * this.cols + col;
			return this.cells.get(index);

		} catch (RangeException e) {
			return new BaseCell(e.getMessage()) {
			};
		}
	}

	/**
	 * Clear data from dataholder <code>cells</code>
	 */
	public void clear() {
		cells.clear();
	}

	/**
	 * Recalculate all cells
	 */
	public void refresh() {
		for (BaseCell cell : cells) {
			cell.setCalculaded(false);
		}
		for (BaseCell cell : cells) {
			if (!cell.isCalculaded())
				cell.getValue();
		}
	}

	/**
	 * Simple output into console
	 */
	public void defaultOutput() {
		int i = 0;
		String tmp = "";
		for (BaseCell cell : cells) {
			tmp = tmp + cell.toString() + "\t";
			i++;
			if (i == this.getCols()) {

				System.out.println(tmp);
				tmp = "";
				i = 0;
			}
		}
	}

	/**
	 * Simple input
	 * 
	 * @param fileName
	 *            - path to file
	 */
	public void defaultInput(String fileName) {
		InputStream inputStream = null;
		BufferedReader bufferedReader = null;
		try {
			inputStream = new FileInputStream(fileName);
			bufferedReader = new BufferedReader(new InputStreamReader(
					inputStream, Charset.forName("UTF-8")));

			String line = "";
			int rows = 0;
			int cols = 0;
			while ((line = bufferedReader.readLine()) != null) {
				String[] strings = line.split("\t");
				rows++;
				for (String value : strings) {
					BaseCell cell = null;

					if (value.isEmpty()) {
						cell = new EmptyCell();
					} else if (value.startsWith("'")) {
						cell = new TextCell();
						cell.setValue(value.substring(1));
					} else if (StringUtils.isNumeric(value)) {
						cell = new NumericCell(Integer.valueOf(value));
					} else if (value.startsWith("=")) {
						cell = new ExpressionCell();
						cell.setExpression(value.substring(1));
					} else {
						throw new NotDefinedCell(value);
					}
					cols = Math.max(cols, strings.length);

					SpreadSheet.getInctance().getCells().add(cell);
				}
			}

			SpreadSheet.getInctance().setRows(rows);
			SpreadSheet.getInctance().setCols(cols);
		}catch(FileNotFoundException e ){	
			System.out.println("File not found !!!");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotDefinedCell e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null)
				try {
					bufferedReader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			bufferedReader = null;
			inputStream = null;
		}

	}

	/**
	 * <p>Return value of cell by cell address, if in address comes numeric value,
	 * method return it, in other case return error value "#unknown"
	 * </p>
	 * <table>
	 * <tr>
	 * <td></td>
	 * <td>A</td>
	 * <td>B</td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td>8</td>
	 * <td>13</td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td>67</td>
	 * <td>8</td>
	 * </tr>
	 * </table>
	 * 
	 * <pre>
	 * String cellValue = SpreadSheet.getCellValueByAddr("A1");  = "8"
	 * String callValue = SpreadSheet.getCellValueByAddr("125"); = "125"
	 * String callValue = SpreadSheet.getCellValueBtAddr("1C");  = "#unknown"
	 * </pre>
	 * 
	 * @param cellAddr
	 *            - address of cell (e.g. "A1", "B2")
	 * @return {@link String}
	 */
	public static String getCellValueByAddr(String cellAddr) {
		if (instance.cells.size() < 1) {
			return "#empty list"; // TODO : Throw error here!!
		}

		try {
			if (StringUtils.isNumeric(cellAddr)) {
				return cellAddr;
			} else if (StringUtils.isAlphanumeric(cellAddr)) {

				char[] numbers = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
						'9' };
				int sepIndex = StringUtils.indexOfAny(cellAddr, numbers);

				if (sepIndex < 0)
					throw new UnknownCellException();

				int comChar = "A".getBytes()[0];
				byte[] charPart = cellAddr.substring(0, sepIndex).toUpperCase()
						.getBytes();
				String numPart = cellAddr.substring(sepIndex);

				int col = 0;
				for (int i = 0; i < charPart.length; i++) {
					col = charPart[i] - comChar + i * 26;
				}

				int row = Integer.valueOf(numPart).intValue() - 1;
				return String.valueOf(SpreadSheet.getInctance()
						.getCell(row, col).getValue());
			} else
				throw new UnknownCellException();
		} catch (UnknownCellException e) {
			return e.getMessage();
		}
	}

}
