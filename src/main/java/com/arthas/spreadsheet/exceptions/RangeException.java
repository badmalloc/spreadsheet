package com.arthas.spreadsheet.exceptions;

@SuppressWarnings("serial")
public class RangeException extends Exception {

	public RangeException() {
		super("#range");	
	}
}
