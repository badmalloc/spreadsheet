package com.arthas.spreadsheet.exceptions;

@SuppressWarnings("serial")
public class UnknownCellException extends Exception {

	public UnknownCellException() {
		super("#unknown");
	}

	
}
