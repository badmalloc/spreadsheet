package com.arthas.spreadsheet.exceptions;

@SuppressWarnings("serial")
public class ExpressionException extends Exception {

	public ExpressionException() {
		super("#expression");
	}

}
