package com.arthas.spreadsheet.exceptions;

@SuppressWarnings("serial")
public class NotDefinedCell extends Exception {

	public NotDefinedCell(String value) {
		super("Not defined cell with value: " + value);
	}

}
