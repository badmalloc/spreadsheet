package com.arthas.spreadsheet;

import org.junit.Test;

import com.arthas.spreadsheet.entity.SpreadSheet;
import com.arthas.spreadsheet.exceptions.NotDefinedCell;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test
	public void testApp() throws NotDefinedCell {
		SpreadSheet sheet = SpreadSheet.getInctance();
		String fileName = getClass().getClassLoader().getResource(".")
				.getPath()
				+ "test.txt";
		sheet.defaultInput(fileName);
		sheet.defaultOutput();
	}

}
