/**
 * 
 */
package com.arthas.spreadsheet;

import static org.junit.Assert.*;

import org.junit.Test;

import com.arthas.spreadsheet.entity.SpreadSheet;

/**
 * @author arthas
 *
 */
public class SpreadSheetTest {

	/**
	 * Test method for {@link com.arthas.spreadsheet.entity.SpreadSheet#getInctance()}.
	 */
	@Test
	public void testGetInctance() {
		SpreadSheet sheet = SpreadSheet.getInctance();
		assertNotNull("Not defined instance of spreadsheet!", sheet);
	}

	/**
	 * Test method for {@link com.arthas.spreadsheet.entity.SpreadSheet#defaultInput(java.lang.String)}.
	 */
	@Test
	public void testDefaultInput() {
		SpreadSheet sheet = SpreadSheet.getInctance();
		String path = getClass().getClassLoader().getResource(".").getPath();
		sheet.defaultInput(path+"test.txt");
		assertTrue("Method 'defaultLoad' doesn`t load data", sheet.getCells().size() > 0);
	}

	/**
	 * Test method for {@link com.arthas.spreadsheet.entity.SpreadSheet#getCellValueByAddr(java.lang.String)}.
	 */
	@Test
	public void testGetCellValueByAddr() {
		SpreadSheet sheet = SpreadSheet.getInctance();
		String path = getClass().getClassLoader().getResource(".").getPath();
		sheet.defaultInput(path+"test.txt");
		
		assertEquals(SpreadSheet.getCellValueByAddr("A1"), "12");		
		assertEquals(SpreadSheet.getCellValueByAddr("K1"), "#range");
		
	}

}
